from FeladatGenerator import FeladatGenerator
from MertekValtasAdat import MertekValtasAdat


class HosszFeladatGenerator(FeladatGenerator):
    def __init__(self):
        FeladatGenerator.__init__(self, [
            MertekValtasAdat(1000000, "mm"),
            MertekValtasAdat(100000, "cm"),
            MertekValtasAdat(10000, "dm"),
            MertekValtasAdat(1000, "m"),
            MertekValtasAdat(1, "km")
        ])
