from FeladatGenerator import FeladatGenerator
from MertekValtasAdat import MertekValtasAdat


class TomegFeladatGenerator(FeladatGenerator):
    def __init__(self):
        FeladatGenerator.__init__(self, [
            MertekValtasAdat(1000000, "g"),
            MertekValtasAdat(100000, "dkg"),
            MertekValtasAdat(1000, "kg"),
            MertekValtasAdat(1, "t")
        ])
