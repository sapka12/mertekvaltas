from FeladatGenerator import FeladatGenerator
from MertekValtasAdat import MertekValtasAdat


class UrtartalomFeladatGenerator(FeladatGenerator):
    def __init__(self):
        FeladatGenerator.__init__(self, [
            MertekValtasAdat(100000, "ml"),
            MertekValtasAdat(10000, "cl"),
            MertekValtasAdat(1000, "dl"),
            MertekValtasAdat(100, "l"),
            MertekValtasAdat(1, "hl"),
        ])
