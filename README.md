# Mértékváltás

## Feladatok és Dicséretek
A projekt véletlenszerű matematikai feladatokat generál, és értékeli a felhasználó válaszait. Az eredménytől függően dicséretek vagy hibaüzenetek jelennek meg.

## Fájlok és Mappák
- mertekvaltas.py: A fő alkalmazás futtatható fájlja.
- TomegFeladatGenerator.py: Tömegfeladatokat generáló modul.
- UrtartalomFeladatGenerator.py: Űrtartalomfeladatokat generáló modul.
- HosszFeladatGenerator.py: Hosszúságfeladatokat generáló modul.

## Telepítés
```
pip install -U pyinstaller
pyinstaller mertekvaltas.py
```
## Használat
```
python3 mertekvaltas.py
```

```
mertekvaltas.exe
```
