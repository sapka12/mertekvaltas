from MertekValtasAdat import MertekValtasAdat


class MertekValtasFeladat:
    def __init__(self, kerdes: MertekValtasAdat, eredmeny: MertekValtasAdat):
        self.kerdes = kerdes
        self.eredmeny = eredmeny

    def tul_nagy_kulonbseg(self):
        kisebb = min(self.kerdes.mennyiseg, self.eredmeny.mennyiseg)
        nagyobb = max(self.kerdes.mennyiseg, self.eredmeny.mennyiseg)
        return nagyobb / kisebb > 1000
