from MertekValtasAdat import MertekValtasAdat
import random

from MertekValtasFeladat import MertekValtasFeladat


class FeladatGenerator:

    def __init__(self, mertekegysegek):
        self.mertekegysegek = mertekegysegek

    def normalizalas(self, egyik: MertekValtasAdat, masik: MertekValtasAdat):
        kisebb_mennyiseg = min(egyik.mennyiseg, masik.mennyiseg)
        return egyik.osztva(kisebb_mennyiseg), masik.osztva(kisebb_mennyiseg)

    def feladat(self):
        _mertekegysegek = random.sample(self.mertekegysegek, k=2)

        egyik, masik = self.normalizalas(_mertekegysegek[0], _mertekegysegek[1])

        kevert = [egyik, masik]
        random.shuffle(kevert)
        sokszorozo = random.randint(1, 19)
        sokszorozott = [
            MertekValtasAdat(sokszorozo * mertekvaltasi_adat.mennyiseg, mertekvaltasi_adat.mertekegyseg)
            for mertekvaltasi_adat in kevert
        ]

        generalt_feladat = MertekValtasFeladat(
            sokszorozott[0],
            sokszorozott[1]
        )

        if generalt_feladat.tul_nagy_kulonbseg():
            return self.feladat()
        else:
            return generalt_feladat
