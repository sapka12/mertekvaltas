class MertekValtasAdat:
    def __init__(self, mennyiseg: int, mertekegyseg: str):
        self.mennyiseg = mennyiseg
        self.mertekegyseg = mertekegyseg

    def osztva(self, i: int):
        return MertekValtasAdat(self.mennyiseg / i, self.mertekegyseg)
