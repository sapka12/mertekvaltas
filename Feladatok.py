import random

from TomegFeladatGenerator import TomegFeladatGenerator
from UrtartalomFeladatGenerator import UrtartalomFeladatGenerator
from HosszFeladatGenerator import HosszFeladatGenerator


class Feladatok:

    def __init__(self):
        self.dicseretek = """
        Kiváló!
        Fantasztikus teljesítmény!
        Kitűnő munka!
        Szuper vagy!
        Ez igazán ügyes megoldás volt!
        Gratulálok, te igazi szakértő vagy!
        Bravó!
        Szép munka!
        Kitartó vagy és jól csinálod!
        Remekül megoldottad a feladatot!
        """

        self.hibauzenetek = """
        Nem baj, próbáld újra!
        Ne add fel, a gyakorlat teszi a mestert!
        Ez még csak a kezdet, ne csüggedj el!
        Mindig van következő alkalom!
        Az eredmény nem mindig számít, a próbálkozás igen!
        Továbbra is kitartóan dolgozhatsz rajta!
        Mindenki hibázik, ez természetes része a tanulásnak!
        A tanulás folyamatos, ne hagyja abba a próbálkozást!
        Még mindig van lehetőség javítani, ne hagyja ki!
        Az egyetlen rossz válasz az, amit nem próbálsz megadni!
        """

    def veletlen(self, lista: str):
        return random.choice([d for d in lista.split("\n") if d.strip()])

    def dicseret(self):
        return self.veletlen(self.dicseretek)

    def hiba(self):
        return self.veletlen(self.hibauzenetek)

    def talalj_feladat_generatort(self, tipus: str):
        if tipus == "urtartalom":
            return UrtartalomFeladatGenerator()
        if tipus == "hosszusag":
            return HosszFeladatGenerator()
        if tipus == "tomeg":
            return TomegFeladatGenerator()
        return random.choice([
            UrtartalomFeladatGenerator(),
            HosszFeladatGenerator(),
            TomegFeladatGenerator()
        ])

    def egy_feladat(self, tipus):
        feladat_generator = self.talalj_feladat_generatort(tipus)

        feladat = feladat_generator.feladat()

        answer = input(
            f"{int(feladat.kerdes.mennyiseg)}{feladat.kerdes.mertekegyseg} =    {feladat.eredmeny.mertekegyseg}\n")

        jo_valasz = int(answer) == feladat.eredmeny.mennyiseg

        if jo_valasz:
            print(self.dicseret())
        else:
            print(self.hiba())

        print(
            f"{int(feladat.kerdes.mennyiseg)}{feladat.kerdes.mertekegyseg} = {int(feladat.eredmeny.mennyiseg)}{feladat.eredmeny.mertekegyseg}")
        print("")
        print("---")
        print("")

        return jo_valasz

    def kezdjuk(self, tipus=None):
        korok = 7
        jo_valaszok = 0
        for _ in range(korok):
            jo_valaszok += int(self.egy_feladat(tipus))

        print(" -------------------------- ")
        print(" -------------------------- ")
        print(" -------------------------- ")

        print("")

        print(f"{jo_valaszok} / {korok}")
        if jo_valaszok == korok:
            print(self.dicseret())
        else:
            print(self.hiba())
